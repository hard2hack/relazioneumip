\documentclass[a4paper,italian,12pt]{article}

\usepackage[hmargin=2.5cm,vmargin=2.5cm,dvips]{geometry}
%%\usepackage{draftcopy}

\usepackage[italian]{babel} % Usa convenzioni tipografiche italiane 
\usepackage[T1]{fontenc} % Usa la nuova codifica standard dei caratteri in
\usepackage{lmodern}
\usepackage[utf8]{inputenc}
\usepackage{enumerate}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{framed}
\usepackage{microtype}
\usepackage[Algoritmo]{algorithm}
\usepackage{algorithmic}
\usepackage{multirow}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{pbox}
\usepackage{colortbl}
\usepackage{tabularx}
\usepackage{subfigure}
\usepackage{graphicx}
\usepackage{fancyvrb}

\frenchspacing


\newcommand{\HRule}[1]{\rule{\linewidth}{#1}} 	% Horizontal rule

\makeatletter							% Title
\def\printtitle{%						
    {\centering \@title\par}}
\makeatother									

\makeatletter							% Author
\def\printauthor{%					
    {\centering \large \@author}}				
\makeatother							

% ------------------------------------------------------------------------------
% Metadata (Change this)
% ------------------------------------------------------------------------------
\title{	\normalsize \textsc{Sicurezza e gestione delle reti di telecomunicazione} 	% Subtitle of the document
		 	\\[2.0cm]													% 2cm spacing
			\HRule{0.5pt} \\										% Upper rule
			\LARGE \textbf{Towards seamless MobileIPv6 handover with UMIP}	
% Title
			\HRule{2pt} \\ [0.5cm]								% Lower rule + 0.5cm spacing
			\normalsize \today									% Todays date
		}

\author{%
		Daniele Baracchi\\
		Federico Toscano\\
		Università degli Studi di Firenze\\	
		Laurea Magistrale in Ingegneria Informatica\\
        \texttt{} \\
}

%%//// document ///////////////////////////////////////////////////////////////

\begin{document}
\newpage
\thispagestyle{empty}				% Remove page numbering on this page

\printtitle									% Print the title data as defined above
  	\vfill
\printauthor

%%//// table of contents //////////////////////////////////////////////////////
\newpage
\tableofcontents

%%//// main text //////////////////////////////////////////////////////////////
\newpage

%% ////////////////////////////////////////////////////////////////////////////
\section{Mobile IPv6}
Questo lavoro si inserisce in un progetto il cui obiettivo è quello di intervenire sulle problematiche che limitano l'integrazione di IPv6 in User Equipment (UE) di nuova generazione. In particolare, in questo lavoro, si è operato su quei vincoli che impediscono ad un dispositivo mobile di poter passare da una rete di accesso ad un’altra (e.g., WiFi o 3G), in modo trasparente ai livelli applicativi, sfruttando appieno gli standard IPv6.

\begin{figure}[h]
	\center
	\includegraphics[width=0.9\textwidth]{img/mip6-scenario.png}
	\caption{Scenario tipico di MobileIPv6}
\end{figure}

Il concetto chiave alla base del \textbf{Mobile IPv6} (rfc 6275) è abbastanza semplice: un host, mentre si muove tra i punti di accesso di reti diverse, ha sempre 2 indirizzi IPv6, uno permanente che identifica il nodo, l'altro varia a seconda della locazione dove esso si trova in un certo istante.

\subsection{Scenario}
Il software che implementa lo standard e che abbiamo studiato è UMIP \cite{umip-site}. Di seguito riportiamo la topologia della rete di testbed e configurazione di MN e HA utilizzata per testarne il funzionamento:

\subsubsection{Configurazione Mobile Node}

\begin{verbatim}
	======================================================================
	NodeConfig MN;	

	DebugLevel 10;	

	OptimisticHandoff enabled;	

	DoRouteOptimizationMN enabled;	

	UseCnBuAck disabled;	

	Interface "eth1" { 
	    MnIfPreference 1;
	}	

	Interface "eth2" { 
	    MnIfPreference 2;
	}
	
	MnHomeLink "eth1" {
	    HomeAgentAddress 2001:760:2c05:1003::1;

	    HomeAddress 2001:760:2c05:1003::a21/64;
	}
	
	UseMnHaIPsec disabled;
	
	KeyMngMobCapability disabled;
	======================================================================
\end{verbatim}
Si vede come si sia impostata l'interfaccia \textit{eth1} come preferita e come si sia indicato l'indirizzo assegnatogli dalla \textit{Home Network} e quelle del suo \textit{Home Agent}.

\subsubsection{Configurazione Home Agent}

\begin{verbatim}
	======================================================================
	NodeConfig HA;	

	DebugLevel 10;
	
	Interface "eth1";
	
	BindingAclPolicy 2001:760:2c05:1003::101 allow;
	BindingAclPolicy 2001:760:2c05:1003::102 allow;
	BindingAclPolicy 2001:760:2c05:1003::a21 allow;
	BindingAclPolicy 2001:760:2c05:1003::1001 allow;
	DefaultBindingAclPolicy deny;
	
	UseMnHaIPsec disabled;

	KeyMngMobCapability disabled;
	======================================================================
\end{verbatim}

\section{Port di UMIP su Android}

\subsection{Patch del Kernel}
I kernel 3.0.72 e 3.4 (usati da Android nelle sue più recenti versioni)
presentano un bug nella gestione Mobile IPv6 che impedisce il corretto
funzionamento di umip. È stato effettuato il backport della patch che corregge
tale bug dal kernel 3.8. Tali patch (android-3.0.72-umip-support.patch e
android-3.4-umip-support.patch) sono disponibi nel repository del progetto
\cite{repo}.

I kernel da patchare sono stati ottenuti da \cite{kernel-repo} e sono stati
compilati con il toolchain fornito da Google \cite{kernel-tc-repo}. Per la
compilazione sono stati seguiti gli step riportati in \cite{kernel-instruct}.

\subsection{Patch di UMIP}
UMIP è stato sviluppato con l'obiettivo di essere facilmente portabile su
sistemi UNIX-like. Sfortunatamente Android non è un ``semplice'' sistema Linux,
e questo ha reso necessario apportare alcune modifiche a umip per consentirne
la compilazione per tale sistema.

Al fine di diminuire al minimo le dipendenze esterne è stato utilizzato il
toolchain dell'Android NDK r9c relativo all'API level 16. Al contrario di altri
toolchain, quello dell'NDK presenta header e librerie garantite essere presenti
su ogni dispositivo Android, consentendoci di ottenere in linea di principio
una compatibilità totale del nostro eseguibile.

È stata esaminata la possibilità di usare altri toolchain per il build di umip,
ma in tutti i casi la strada si è rivelata essere impraticabile. In particolare
il toolchain usato per compilare il kernel non contiene alcuna implementazione
di una libc, mentre quello di CodeSourcery utilizza glibc - costringendo quindi
a compilare staticamente l'eseguibile e senza fornire garanzie di funzionamento
a causa delle differenze tra gnueabi e androideabi.

Sono disponibili diverse altre toolchain oltre a quella dell'NDK in grado di
generare binari eseguibili su Android. Sebbene tali toolchain siano più vicine

Le estensioni del kernel richieste per UMIP, quando abilitate, esportano in
userspace diverse strutture dati e costanti, che tuttavia non sono presenti
negli header forniti con il toolchain dell'NDK. Sono stati aggiunti due header
di supporto (android\_support.h e android\_mip\_support.h) a UMIP contenenti
tali definizioni al fine di correggere questa lacuna.

Android usa come libreria C Bionic anziché Glibc. Tale libreria presenta alcune
peculiarità rispetto alle librerie C normalmente disponibili sui sistemi
UNIX-like, tra cui la mancanza di alcune costanti usate da UMIP, che sono state
quindi aggiunte tramite un header di supporto. L'implementazione di pthread,
inoltre, è inclusa direttamente all'interno della libc, pur senza alcune
caratteristiche usate da UMIP (e.g. thread cancellabili). Per le funzioni
mancanti sono stati creati degli stub che consentono la compilazione e
l'esecuzione del software, anche se con qualche problema (e.g. attesa di un
timeout di 50 secondi all'avvio prima della creazione del tunnel). È stato
necessario infine importare dalla libc NetBSD la funzione di pattern matching
``glob'', assente in Bionic.

Data la grande quantità di modifiche necessarie per far funzionare UMIP su
Android è stato modificato il file autoconf per prevedere una nuova opzione:
``--with-android-support''. Specificando tale opzione viene definita la
costante del preprocessore ANDROID\_SUPPORT\_REQUIRED, che a sua volta abilita
l'inclusione degli header di supporto dove necessario. Vengono inoltre aggiunti
al Makefile i sorgenti della funzione glob.

\section{Handover asincrono}
Parallelamente al porting è stato studiato il comportamento di UMIP per quanto riguarda il setup iniziale e le fasi di handover tra reti differenti.
Lo scopo principale di questo progetto infatti è stato il cercare di rendere possibile il passaggio di un MN da una rete all'altra in maniera asincrona, o meglio dare la possibilità ai livelli superiori dell'architettura android di scegliere a quale rete connettersi in base a criteri, espressi all'interno tali livelli, invece di attendere che il nodo si disconnetta dalla rete corrente e si connetta ad un'altra.

\subsection{Setup}
Attraverso le procedure di \textit{router advertisement} gli agent delle reti \textit{home} e \textit{foreign} vengono messi a conoscenza di quali \textit{Care-of-Address} sono responsabili, in modo tale da sapere verso quale router instradare i pacchetti indirizzati ad un certo indirizzo.

Se il \textit{Mobile Node} inizialmente si trova nella home network allora, dopo il setup fatto precedentemente, è richiesto solo il processo di discovery del \textit{Home Agent} e il MN riesce a comunicare correttamente con la rete.
Se invece il MN si trova in foreign network viene effettuato uno scambio di informazioni fra il MN e l'HA che prevede il binding del CoA con HoA e l'instaurazione di un tunnel fra essi, attraverso il quale avvengono tutte le comunicazioni originariamente destinate al MN sul suo indirizzo di home network.
Per una descrizione più dettagliata di queste comunicazioni con flow-charts si rimanda a \cite{mipv6-flow}

\subsection{Handover}

Quando il MN passa da una rete ad un altra UMIP attiva delle procedure di handover che vanno a modificare le tabelle di routing delle entità coinvolte.
UMIP gestisce il controllo del verificarsi di questo tipo di eventi con un sistema abbastanza complesso di timer e macchine a stati. Inoltre è previsto un meccanismo di preferenza delle interfacce di rete che permette al software di scegliere quale interfaccia usare nel caso che siano tutte connesse alle rispettive reti.

Si è cercato quindi di studiare il comportamento di UMIP che effettua il trigger dell'handover, così da cercare di inserirsi in queste procedure e attivarle in maniera asincrona da altre parti di software.

Purtroppo data la complessità delle interazioni fra i due meccanismi (timer controllo eventi e preferenza interfaccia) non è stato possibile modificare queste procedure dall'interno, quindi si è scelta la strada di forzare questi eventi attraverso un'interfaccia programmata. Nel nostro caso si è trattato di chiamare l'utilità \texttt{svc}, mediante la quale si accende e si spenge l'interfaccia wireless (fig.\ref{fig-socket-diagram}).
\begin{figure}[h]
	\center
	\includegraphics[width=0.9\textwidth]{img/android-diagram.png}
	\caption{Diagramma delle entità coinvolte nel funzionamento di Umip. In rosso la chiamata proveniente dall'Application Framework diretto alla socket}
	\label{fig-socket-diagram}
\end{figure}
Si è dunque predisposto un thread all'interno di UMIP che ascolti su una socket locale in attesa di istruzioni, impartite dai livelli superiori.

\newpage
Per l'implementazione si rimanda al repository \cite{repo}. Nel file \texttt{open\_server\_socket.c} è contenuta la funzione che crea una socket locale e che viene chiamata all'interno di un thread diverso. Nel file \texttt{java\_client.java} è stata riportata una classe java che esegue una connessione alla socket locale creata, e che impartisce le operazioni necessarie. E' stato scelto java poiché tale funzionalità dovrà essere implementata nell'\textit{Application Framework}.

%
%\begin{itemize}
%	\item \textbf{Neighbour discovering} le entità della rete IPv6 vengono a conoscenza 
%	\item Router advertising
%	\item Assegnazione del CoA
%\end{itemize}

\newpage
\cleardoublepage
\addcontentsline{toc}{chapter}{Bibliografia}
\bibliographystyle{plain}
\bibliography{bibliografia}
\end{document}

