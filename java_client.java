import java.io.IOException;
import java.net.InetSocketAddress;
import java.nio.channels.SocketChannel;
import java.nio.ByteBuffer;

public class Client {
	public static String MOCKRIL_ADDR_STR = "127.0.0.1";
	public static int MOCKRIL_PORT = 53777;
	public static SocketChannel mChannel = null;
	
	static public void open() throws IOException {
		InetSocketAddress mockRilAddr = new InetSocketAddress(
								MOCKRIL_ADDR_STR, MOCKRIL_PORT);
		mChannel= SocketChannel.open(mockRilAddr);
	}
	
	static public void close() {
		try {
			if (mChannel != null) {
				mChannel.close();
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	static public final int sendAll(ByteBuffer bb) throws IOException {
		int count = 0;
		while (bb.remaining() != 0) {
			count += mChannel.write(bb);
		}
		return count;
	}
	
	static public final int recvAll(ByteBuffer bb) throws IOException {
		int count = 0;
		while (bb.remaining() != 0) {
			count += mChannel.read(bb);
		}
		return count;
	}
	
   public static void main(String[] args){
		try {
			open();
			String command_str = "command";
			ByteBuffer data = ByteBuffer.wrap(command_str.getBytes());
			sendAll(data);
		} catch (IOException e) {
			e.printStackTrace();
		}      
	}
}