static int *open_server_socket()
{
	int port = 53777;
	char buf [20];
	char enable_home_iface_buf[20] = "enable_home_iface";
	char disable_home_iface_buf[20] = "disable_home_iface";
	char close_socket_buf[20] = "close_socket";
	int s, newsock;
	struct sockaddr_in addr, from;

	int exit_cmd_rec = 0;

	s = socket(AF_INET, SOCK_STREAM, 0);
	if (s < 0) {
		perror("opening stream socket");
		return 0;
	}

	addr.sin_family = AF_INET;
	addr.sin_port = htons(port);
	addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);

	bind(s, (struct sockaddr*)&addr, sizeof(addr));
	listen(s, 5);
	dbg("Ready for client connect().\n");
	while(!exit_cmd_rec) {
		memset(&buf[0], 0, sizeof(buf));
		int fromlen = sizeof (from);
		newsock = accept(s, (struct sockaddr *)&from, &fromlen);
		dbg("New connection.\n");

		recv(newsock, buf, sizeof (buf), 0);
		if(strcmp(buf,enable_home_iface_buf) == 0){
			dbg("enabling home iface\n");
			system("svc wifi enable");
		} else if(strcmp(buf,disable_home_iface_buf) == 0){
			dbg("disabling home iface\n");
			system("svc wifi disable");
		} else if(strcmp(buf,close_socket_buf)){
			dbg("closing socket\n");
			exit_cmd_rec = 1;
		} else {
				dbg("command not recognised (%s)\n",buf);
		}

		close(newsock);
	}
	close(s);
	return 0;
}